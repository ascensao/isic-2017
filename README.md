# ISIC 2017

This repo contains a script to read the data from the ISIC 2017 into Tensorflow.

Further work (not addressed at the moment):
* Masking the images, using the ground truth from the first part of the challenge.

```
.
├── data
│   ├── ISIC-2017_Training_Data
│   ├── ISIC-2017_Training_Part1_GroundTruth
│   └── ISIC-2017_Training_Part3_GroundTruth.csv
├── notebooks
│   └── temp-jascensao-load_images.ipynb
├── README.md
├── requirements.txt
├── src
│   └── read_images.py

```
