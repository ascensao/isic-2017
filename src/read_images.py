import os

import pandas as pd
import tensorflow as tf


DATA_DIR = os.path.join("..", "data")
IMAGE_DIR = os.path.join(DATA_DIR, "ISIC-2017_Training_Data")

FILENAME = "ISIC-2017_Training_Part3_GroundTruth.csv"
DATASET_PATH = os.path.join(DATA_DIR, FILENAME)

# The number of classes in the dataset.
# In this case we have three: nothing, melanoma or seborrheic keratosis.
N_CLASSES = 3

IMG_HEIGHT = 64
IMG_WIDTH = 64

# The 3 color channels in the images.
# Change to 1 if grayscale.
CHANNELS = 3


def read_images(dataset_path, image_dir, batch_size, img_height, img_width):
    """Read images from disk and into Tensorflow.

    This functions reads image ids and labels from a `.csv` file, located in
    `dataset_path`. Then, it builds the image paths using the image ids and the
    `image_dir`. Finally, it reads the images from disk, resizes them, and then
    it creates minibatches for training.

    Args:
        dataset_path (str): path to `ISIC-2017_Training_Part3_GroundTruth.csv`.
        image_dir (str): path to the `ISIC-2017_Training_Data` folder.
        batch_size (int): Size of microbatches for training.
        img_height (int): The image height, after resizing.
        img_width (int): The image width, after resizing.

    Returns:
        X (tensor): Tensor with images, in microbatches.
        Y (tensor): Tensor with labels, in microbatches.

    """

    imagepaths, labels = read_dataset_from_file_and_convert_to_tensor(
        dataset_path, image_dir
    )

    image, label = build_tensorflow_queue(imagepaths, labels)

    image = read_images_from_disk(image)
    image = resize_images_to_a_common_size(image, img_height, img_width)
    image = normalize_image(image)

    X, Y = create_batches(image, label, batch_size)

    return X, Y


def read_dataset_from_file_and_convert_to_tensor(dataset_path, image_dir):
    """Read dataset file and convert it to tensor.

    Read the dataset file in `dataset_path`, build the images paths from the
    image ids and the `image_dir`, and the labels. Convert the lists of images
    and labels into tensors.

    Args:
        dataset_path (str): path to `ISIC-2017_Training_Part3_GroundTruth.csv`.
        image_dir (str): path to the `ISIC-2017_Training_Data` folder.

    Returns:
        (tensor, tensor): Two tensors, the first containing paths to images and
            the sencond one the labels.

    """
    imagepaths, labels = read_dataset_file(dataset_path, image_dir)

    return convert_to_tensor(imagepaths, labels)


def read_dataset_file(dataset_path, image_dir):
    """Read the dataset file and extract imagepaths and labels.

    Read the `ISIC-2017_Training_Part3_GroundTruth.csv` file, create a new
    column with the imagepaths, built by appending the image id to the
    `image_dir`, and other with the labels (1 for melanoma, 2 for seborrheic
    keratosis, and 0 for nothing). It returns the imagepaths and the labels.

    Args:
        dataset_path (str): path to `ISIC-2017_Training_Part3_GroundTruth.csv`.
        image_dir (str): path to the `ISIC-2017_Training_Data` folder.

    Returns:
        (list, list): Two lists, one with imagepaths, the other with the
            corresponding labels.

    """
    data = pd.read_csv(dataset_path)

    data = data.assign(
        imagepath=data["image_id"].apply(
            lambda x: os.path.join(image_dir, x + ".jpg")
        ),
        label=data.apply(build_label, axis=1),
    )

    imagepaths = data["imagepath"].tolist()
    labels = data["label"].tolist()

    return imagepaths, labels


def build_label(row):
    """Build the labels from the dataset file.

    The `ISIC-2017_Training_Part3_GroundTruth.csv` contains two columns with
    the labels: `melanoma` and `seborrheic_keratosis`. When both are zero, it
    means there is nothing in the image.

    Args:
        row (pd.Series): a row of the dataframe, corresponding to an image.

    Returns:
        (int): The image's label, 0 for nothing, 1 for melanoma and 2 for the
        seborrheic_keratosis.

    """
    if row["melanoma"] == 1:
        return 1
    elif row["seborrheic_keratosis"] == 1:
        return 2
    else:
        return 0


def convert_to_tensor(imagepaths, labels):
    """Convert imagepaths and labels from lists to tensors.

    Receives the lists containing the imagepaths and the labels for all the
    corresponding images and converts both lists to tensors, for downstream
    processing.

    Args:
        imagepaths (list): The list with the paths for all images.
        labels (list): The list with the labels for all images.

    Returns:
        (tensor, tensor): Paths and labels for all images as tensors.

    """
    imagepaths = tf.convert_to_tensor(imagepaths, dtype=tf.string)
    labels = tf.convert_to_tensor(labels, dtype=tf.int32)

    return imagepaths, labels


def build_tensorflow_queue(imagepaths, labels):
    """Build a list of tensors, one for each element of imagepaths and labels.

    Receive the imagepaths and labels tensors, and build a list of individual
    images and labels as tensor objects.

    Args:
        imagepaths (tensor): tensor containing all the image paths.
        labels (tensor): tensor containing all the corresponding labels.

    Returns:
        (tensor, tensor): imagepaths and labels as queues of tensors.

    """
    return tf.train.slice_input_producer([imagepaths, labels], shuffle=True)


def read_images_from_disk(image):
    """Read the images from disk.

    Use the list (queue) of imagepaths to read the images from disk and
    decode them, using the `decode_jpeg` to decode a JPEG-encoded image to a
    tensor of integers.

    Args:
        image (tensors): The tensor containing the imagepath.

    Returns:
        (tensor): A list of tensors of type int.

    """
    image = tf.read_file(image)

    return tf.image.decode_jpeg(image, channels=CHANNELS)


def resize_images_to_a_common_size(image, img_height, img_width):
    """Resize images.

    Receive a tensor representing images and resize them to a common size.

    Args:
        image (tensor): The image tensor.
        img_height (int): The image height, after resizing.
        img_width (int): The image width, after resizing.

    Returns:
        (tensor): A list of resizes images as tensors of type int.

    """
    return tf.image.resize_images(image, [img_height, img_width])


def normalize_image(image):
    """Normalize image.

    Normalize all images to a common scale.

    Args:
        image (tensor): The image tensor.

    Returns:
        (tensor): Scaled image.

    """
    return image * 1.0 / 127.5 - 1.0


def create_batches(image, label, batch_size):
    """Create minibatches.

    Partition images in batches of size `batch_size` for minibatch training.

    Args:
        image (tensor): The image tensor.
        label (tensor): The label tensor.
        batch_size (int): Size of microbatches for training.

    Returns:
        (tensor, tensor): The image and label tensors, in minibatches.

    """
    return tf.train.batch(
        [image, label],
        batch_size=batch_size,
        capacity=batch_size * 8,
        num_threads=4,
    )
